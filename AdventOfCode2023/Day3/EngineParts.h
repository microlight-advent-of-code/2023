#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int number_from_string(const std::vector<std::string>& lines, const int lineIndex, const int numberStartID, const int numberEndID) {
    std::string numberString;
    for (int i = numberStartID; i <= numberEndID; i++) {
        numberString += lines[lineIndex][i];
    }
    return std::stoi(numberString);
}
int find_number_end_index(const std::string& line, int index) {
    while (index < line.length()) {
        if (isdigit(line[index])) {
            index++;
            continue;
        }
        index--;
        break;
    }
    if (index >= line.length())
        index = line.length() - 1;
    return index;
}
bool check_if_number_is_near_symbol(const std::vector<std::string>& lines, const int lineIndex, const int numberStartID, const int numberEndID) {
    //std::cout << "Checking if number " << number_from_string(lines, lineIndex, numberStartID, numberEndID) << " is near symbol." << std::endl;
    // Check left
    if (numberStartID > 0) {
        if (lines[lineIndex][numberStartID - 1] != '.') {
            //std::cout << "Found symbol " << lines[lineIndex][numberStartID - 1] << std::endl;
            return true;
        }
    }
    // Check right
    if (numberEndID < lines[lineIndex].length() - 1) {
        if (lines[lineIndex][numberEndID + 1] != '.') {
            //std::cout << "Found symbol " << lines[lineIndex][numberEndID + 1] << std::endl;
            return true;
        }
    }
    // Check above
    if (lineIndex > 0) {
        for (int i = numberStartID - 1; i <= numberEndID + 1; i++) {
            if (i >= 0 && i < lines[lineIndex - 1].length() && lines[lineIndex - 1][i] != '.') {
                //std::cout << "Found symbol " << lines[lineIndex - 1][i] << std::endl;
                return true;
            }
        }
    }
    // Check below
    if (lineIndex < lines.size() - 1) {
        for (int i = numberStartID - 1; i <= numberEndID + 1; i++) {
            if (i >= 0 && i < lines[lineIndex + 1].length() && lines[lineIndex + 1][i] != '.') {
                //std::cout << "Found symbol " << lines[lineIndex + 1][i] << std::endl;
                return true;
            }
        }
    }
    //std::cout << number_from_string(lines, lineIndex, numberStartID, numberEndID) << " is not near symbol." << std::endl;
    return false;
}
void find_next_number(const std::vector<std::string>& lines, int& lineIndex, int& numberStartID, int& numberEndID) {
    //std::cout << "Need to find next number" << std::endl;
    while (true) {
        if (lineIndex >= lines.size()) {
            lineIndex = -1;
            numberStartID = -1;
            numberEndID = -1;
            //std::cout << "Reached end of the file!" << std::endl;
            return;
        }
        for (numberStartID; numberStartID <= lines[lineIndex].length(); numberStartID++) {
            if (isdigit(lines[lineIndex][numberStartID])) {
                numberEndID = find_number_end_index(lines[lineIndex], numberStartID);
                //std::cout << "Found number! Start index is " << numberStartID << "; end index is: " << numberEndID << "; Number is: " << number_from_string(lines, lineIndex, numberStartID, numberEndID) << std::endl;
                return;
            }
        }
        //std::cout << "Going to new line, start index: " << numberStartID << "; end index; " << numberEndID << std::endl;
        lineIndex++;
        numberStartID = 0;
    }
}
void sum_engine_parts(const std::vector<std::string>& lines, int& sum) {
    int lineIndex = 0;
    int numberStartID = 0;
    int numberEndID = 0;

    find_next_number(lines, lineIndex, numberStartID, numberEndID);
    while (numberEndID != -1) {
        if (check_if_number_is_near_symbol(lines, lineIndex, numberStartID, numberEndID)) {
            //std::cout << "+++ADDING " << number_from_string(lines, lineIndex, numberStartID, numberEndID) << " to the sum." << std::endl;
            sum += number_from_string(lines, lineIndex, numberStartID, numberEndID);
        }
        numberStartID = numberEndID + 1;
        find_next_number(lines, lineIndex, numberStartID, numberEndID);
    }
}