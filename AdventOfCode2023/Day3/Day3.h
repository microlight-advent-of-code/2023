#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Day3/EngineParts.h"
#include "Day3/GearRatios.h"

void day_3() {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Starting Day 3 program!" << std::endl;

    std::fstream file;
    std::vector<std::string> lines;
    
    int sum = 0;
    int gearSum = 0;

    file.open("Day3/input.txt", std::ios::in);
    //file.open("Day3/testinput.txt", std::ios::in);

    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            lines.push_back(line);
        }
        file.close();
    }

    sum_engine_parts(lines, sum);
    sum_gear_ratios(lines, gearSum);
    std::cout << "Result is: " << sum << std::endl;
    std::cout << "Gear ratio sum is: " << gearSum << std::endl;
}