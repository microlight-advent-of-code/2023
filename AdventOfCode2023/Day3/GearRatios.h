#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int find_number(const std::vector<std::string>& lines, const int lineIndex, const int index, int& counter) {
    std::string numberString;
    numberString += lines[lineIndex][index];

    int i = -1;
    while (true) {
        if (index + i < 0) break;
        if (isdigit(lines[lineIndex][index + i])) {
            numberString = lines[lineIndex][index + i] + numberString;
            i--;
            continue;
        }
        break;
    }
    i = 1;
    while (true) {
        if (i + index >= lines[lineIndex].length()) break;
        if (isdigit(lines[lineIndex][index + i])) {
            numberString += lines[lineIndex][index + i];
            counter++;
            i++;
            continue;
        }
        break;
    }
    //std::cout << "Found number " << numberString << std::endl;
    return std::stoi(numberString);
}
int try_to_add_gear_numbers(const std::vector<std::string>& lines, const int lineIndex, const int gearIndex) {
    std::vector<int> numbers;

    // Check left
    if (gearIndex > 0) {
        if (isdigit(lines[lineIndex][gearIndex - 1])) {
            std::string numberString;
            int i = -1;
            while (true) {
                if (gearIndex + i < 0) break;
                if (!isdigit(lines[lineIndex][gearIndex + i])) break;

                numberString = lines[lineIndex][gearIndex + i] + numberString;
                i--;
            }
            numbers.push_back(std::stoi(numberString));
        }
    }
    // Check right
    if (gearIndex < lines[lineIndex].length() - 1) {
        if (isdigit(lines[lineIndex][gearIndex + 1])) {
            std::string numberString;
            int i = 1;
            while (true) {
                if (gearIndex + i < 0) break;
                if (!isdigit(lines[lineIndex][gearIndex + i])) break;

                numberString += lines[lineIndex][gearIndex + i];
                i++;
            }
            numbers.push_back(std::stoi(numberString));
        }
    }
    // Check up
    if (lineIndex - 1 >= 0) {
        for (int i = -1; i <= 1; i++) {
            if (isdigit(lines[lineIndex - 1][gearIndex + i])) {
                numbers.push_back(find_number(lines, lineIndex - 1, gearIndex + i, i));
                i++;
            }
        }
    }
    // Check down
    if (lineIndex + 1 < lines.size()) {
        for (int i = -1; i <= 1; i++) {
            if (isdigit(lines[lineIndex + 1][gearIndex + i])) {
                numbers.push_back(find_number(lines, lineIndex + 1, gearIndex + i, i));
                i++;
            }
        }
    }

    if (numbers.size() >= 2) {
        return numbers[0] * numbers[1];
    }    
    return 0;
}
void find_next_gear(const std::vector<std::string>& lines, int& lineIndex, int& gearIndex) {
    //std::cout << "Need to find next gear" << std::endl;
    while (true) {
        if (lineIndex >= lines.size()) {
            lineIndex = -1;
            gearIndex = -1;
            //std::cout << "Reached end of the file!" << std::endl;
            return;
        }
        for (gearIndex; gearIndex <= lines[lineIndex].length(); gearIndex++) {
            if (lines[lineIndex][gearIndex] == '*') {
                //std::cout << "Found gear! Index is " << gearIndex << "; at line " << lineIndex << std::endl;
                return;
            }
        }
        lineIndex++;
        gearIndex = 0;
    }
}
void sum_gear_ratios(const std::vector<std::string>& lines, int& gearSum) {
    int lineIndex = 0;
    int gearIndex = 0;

    find_next_gear(lines, lineIndex, gearIndex);
    while (lineIndex != -1) {         
        gearSum += try_to_add_gear_numbers(lines, lineIndex, gearIndex);
        gearIndex++;
        find_next_gear(lines, lineIndex, gearIndex);
    }
}