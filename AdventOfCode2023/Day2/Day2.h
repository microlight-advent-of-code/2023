#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

#include "Constants.h"

struct setOfCubes {
public:
	int red;
	int green;
	int blue;

    setOfCubes(int r, int g, int b) : red(r), green(g), blue(b) {}
    setOfCubes(){
    }
};
setOfCubes lowestPossible;

bool is_set_valid(setOfCubes& set) {
    if (set.red > 12) return false;
    if (set.green > 13) return false;
    if (set.blue > 14) return false;
    return true;
}
void add_cubes(setOfCubes& set, std::string& word) {
    int length = 0;
    int digit;
    std::string color;

    for (int i = 0; i < word.length(); i++) {
        if (isdigit(word[i])) {
            length++;
            continue;
        }
        break;
    }

    digit = std::stoi(word.substr(0, length));
    color = word.substr(length);

    if (color == "red") {
        set.red = digit;
        if (lowestPossible.red < set.red) lowestPossible.red = set.red;
    }
    else if (color == "green") {
        set.green = digit;
        if (lowestPossible.green < set.green) lowestPossible.green = set.green;
    }
    else if (color == "blue") {
        set.blue = digit;
        if (lowestPossible.blue < set.blue) lowestPossible.blue = set.blue;
    }
}
setOfCubes build_set_from_string(std::string& word) {
    std::vector<std::string> separatedWord;
    separatedWord = Constants::splitString(word, ',');
    setOfCubes set = { 0, 0, 0 };

    for (int i = 0; i < separatedWord.size(); i++) {
        add_cubes(set, separatedWord[i]);
    }
    return set;
}
bool check_string_set(std::string& set) {
    setOfCubes setCubes = build_set_from_string(set);
    return is_set_valid(setCubes);
}
void day_2() {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Starting Day 2 program!" << std::endl;

	std::fstream file;
    std::string line;
    std::vector<std::string> firstSplit;
    std::vector<std::string> sets;
    lowestPossible = setOfCubes(0, 0, 0);
    
    int currentID = 0;
    int sum = 0;
    int powerSum = 0;
    bool goodSet;

    file.open("Day2/input.txt", std::ios::in);

    if (file.is_open()) {
        while (getline(file, line)) {
            currentID++;
            line.erase(std::remove_if(line.begin(), line.end(), ::isspace), line.end());
            firstSplit = Constants::splitString(line, ':');
            sets = Constants::splitString(firstSplit[1], ';');
            goodSet = true;
            lowestPossible = setOfCubes(1, 1, 1);
            for (int i = 0; i < sets.size(); i++) {
                if (!check_string_set(sets[i])) {
                    goodSet = false;
                }
            }
            if (goodSet) {
                sum += currentID;                
            }
            powerSum += lowestPossible.red * lowestPossible.green * lowestPossible.blue;
        }
        file.close();        
    }

    std::cout << "Result is: " << sum << std::endl;
    std::cout << "Sum of power is: " << powerSum << std::endl;
}