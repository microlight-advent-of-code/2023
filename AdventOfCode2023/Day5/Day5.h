#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Constants.h"

struct map {
public:
    unsigned int destinationStart;
    unsigned int sourceStart;
    unsigned int sourceEnd;
    unsigned int range;

    map(unsigned int destinationStart, unsigned int sourceStart, unsigned int range) {
        this->destinationStart = destinationStart;
        this->sourceStart = sourceStart;
        sourceEnd = sourceStart + range - 1;
        this->range = range;
    }
    map() {
    }

    int calculate_seed(const unsigned int& seed) {
        if (seed < sourceStart || seed > sourceEnd) {
            return seed;
        }
        return seed - sourceStart + destinationStart;
    }
};
struct groups_group {
public:
    std::vector<map> seed_to_soil;
    std::vector<map> soil_to_fertilizer;
    std::vector<map> fertilizer_to_water;
    std::vector<map> water_to_light;
    std::vector<map> light_to_temperature;
    std::vector<map> temperature_to_humidity;
    std::vector<map> humidity_to_location;

    groups_group(){}
};
int calculate_seed_from_map(const unsigned int& seed, const std::vector<map>& map_group) {
    int newSeed;
    for (map x : map_group) {
        newSeed = x.calculate_seed(seed);
        if (newSeed != seed) {
            return newSeed;
        }
    }
    return seed;
}
unsigned int process_seeds(const std::vector<unsigned int>& seeds, const groups_group& all_groups) {
    unsigned int smallest = 3999999999;
    for (int i = 0; i * 2 < seeds.size(); i++) {
        std::cout << "Checking for new seed set " << i * 2 << std::endl;
        for (int j = 0; j < seeds[i * 2 + 1]; j++) {
            //std::cout << "Checking for seed " << seeds[i*2] + j << std::endl;
            unsigned int newSeed = calculate_seed_from_map(seeds[i * 2] + j, all_groups.seed_to_soil);
            newSeed = calculate_seed_from_map(newSeed, all_groups.soil_to_fertilizer);
            newSeed = calculate_seed_from_map(newSeed, all_groups.fertilizer_to_water);
            newSeed = calculate_seed_from_map(newSeed, all_groups.water_to_light);
            newSeed = calculate_seed_from_map(newSeed, all_groups.light_to_temperature);
            newSeed = calculate_seed_from_map(newSeed, all_groups.temperature_to_humidity);
            newSeed = calculate_seed_from_map(newSeed, all_groups.humidity_to_location);
            //std::cout << "New seed is " << newSeed << std::endl;
            if (newSeed < smallest) {
                //std::cout << "+++++IT IS SMALLER!" << std::endl;
                smallest = newSeed;
            }
        }
    }
    return smallest;
}
void fill_seeds(std::vector<unsigned int>& seeds, const std::string& line) {
    std::vector<std::string> firstSplit = Constants::splitString(line, ':');
    firstSplit[1] = Constants::trim_spaces_edges(firstSplit[1]);
    std::vector<std::string> seedsInStrings = Constants::splitString(firstSplit[1], ' ');

    for (std::string x : seedsInStrings) {
        seeds.push_back(std::stoul(x));
    }
}
void add_map(std::vector<map>& list, const std::string& map_group) {
    std::vector<std::string> parameters = Constants::splitString(map_group, ' ');
    list.push_back(map(std::stoul(parameters[0]), std::stoul(parameters[1]), std::stoul(parameters[2])));
}
void day_5() {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Starting Day 5 program!" << std::endl;

    std::fstream file;
    std::vector<unsigned int> seeds;
    groups_group all_groups = groups_group();

    file.open("Day5/input.txt", std::ios::in);
    //file.open("Day5/testinput.txt", std::ios::in);

    if (file.is_open()) {
        std::string line;
        bool skip_next_line = false;
        int fill_index = 0;
        while (getline(file, line)) {
            if (skip_next_line) {
                skip_next_line = false;
                continue;
            }            
            if (line.empty()) {
                fill_index++;
                skip_next_line = true;
                continue;
            }
            if (fill_index == 0) {
                fill_seeds(seeds, line);
                continue;
            }
            else if (fill_index == 1) {
                add_map(all_groups.seed_to_soil, line);
            }
            else if (fill_index == 2) {
                add_map(all_groups.soil_to_fertilizer, line);
            }
            else if (fill_index == 3) {
                add_map(all_groups.fertilizer_to_water, line);
            }
            else if (fill_index == 4) {
                add_map(all_groups.water_to_light, line);
            }
            else if (fill_index == 5) {
                add_map(all_groups.light_to_temperature, line);
            }
            else if (fill_index == 6) {
                add_map(all_groups.temperature_to_humidity, line);
            }
            else if (fill_index == 7) {
                add_map(all_groups.humidity_to_location, line);
            }
            else {
                break;
            }
        }
        file.close();
    }

    unsigned int smallestNumber = process_seeds(seeds, all_groups);

    std::cout << "Result is: " << smallestNumber << std::endl;
}