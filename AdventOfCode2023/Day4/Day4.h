#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Constants.h"

struct card {
public:
    int copies;
    std::vector<int> winningNumbers;
    std::vector<int> pickedNumbers;

    card(const std::string& cardInString) {
        std::vector<std::string> firstSplit = Constants::splitString(cardInString, ':');
        std::vector<std::string> cardSides = Constants::splitString(firstSplit[1], '|');

        std::string stringOfWinningNumbers = Constants::trim_spaces_edges(cardSides[0]);
        stringOfWinningNumbers = Constants::removeDuplicateSpaces(stringOfWinningNumbers);
        std::vector<std::string> winningNumbersStrings = Constants::splitString(stringOfWinningNumbers, ' ');
        for (std::string s : winningNumbersStrings) {
            winningNumbers.push_back(std::stoi(s));
        }

        std::string stringOfPickedNumbers = Constants::trim_spaces_edges(cardSides[1]);
        stringOfPickedNumbers = Constants::removeDuplicateSpaces(stringOfPickedNumbers);
        std::vector<std::string> pickedNumbersStrings = Constants::splitString(stringOfPickedNumbers, ' ');
        for (std::string s : pickedNumbersStrings) {
            pickedNumbers.push_back(std::stoi(s));
        }

        copies = 1;
    }
    card(const card& inputCard) {
        winningNumbers = inputCard.winningNumbers;
        pickedNumbers = inputCard.pickedNumbers;

        copies = 1;
    }
    card(){}
};
int calculate_points(const card& inputCard) {
    int points = 0;

    for (auto it = inputCard.pickedNumbers.begin(); it != inputCard.pickedNumbers.end(); it++) {
        if (Constants::contains_element(inputCard.winningNumbers, *it)) {
            /*if (points == 0) {
                points++;
            }
            else {
                points *= 2;
            }*/
            points++;
        }        
    }

    return points;
}
void process_cards(std::vector<card>& cards, int& sum) {
    for (int i = 0; i < cards.size(); i++) {
        int cardsToInsert = calculate_points(cards[i]);
        while (cardsToInsert > 0) {
            cards[i + cardsToInsert].copies += cards[i].copies;
            cardsToInsert--;
        }
    }
    for (card& c : cards) {
        sum += c.copies;
    }
}
void day_4() {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Starting Day 4 program!" << std::endl;

    std::fstream file;
    std::vector<card> cards;

    int sum = 0;

    file.open("Day4/input.txt", std::ios::in);
    //file.open("Day4/testinput.txt", std::ios::in);

    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            cards.push_back(card(line));
            //sum += calculate_points(cards[cards.size() - 1]);
        }
        file.close();
    }
    process_cards(cards, sum);

    std::cout << "Result is: " << sum << std::endl;
}