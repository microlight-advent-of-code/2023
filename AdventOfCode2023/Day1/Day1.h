#pragma once
#include <iostream>
#include <fstream>
#include <string>

#include "Constants.h"

int check_if_word_is_number(std::string word) {
    for (int i = 0; i < Constants::NUMBERS_AS_WORDS.size(); i++) {
        if (Constants::NUMBERS_AS_WORDS[i] == word) {
            return i + 1;
        }
    }
    return 0;
}

char disassemble_string(std::string wordCheck, bool fromStart) {
    if (wordCheck.length() < 3) return 'a';

    int startIndex;
    int returnedInt;
    int length = wordCheck.length();

    if (fromStart) {
        startIndex = 0;
        while (length - startIndex >= 3) {
            std::string checkString = "";
            checkString += wordCheck[startIndex];
            checkString += wordCheck[startIndex + 1];
            checkString += wordCheck[startIndex + 2];

            returnedInt = check_if_word_is_number(checkString);
            if (returnedInt != 0) {
                return returnedInt;
            }
            if (length - startIndex >= 4) {
                checkString += wordCheck[startIndex + 3];
                returnedInt = check_if_word_is_number(checkString);
                if (returnedInt != 0) {
                    return returnedInt;
                }
            }
            if (length - startIndex >= 5) {
                checkString += wordCheck[startIndex + 4];
                returnedInt = check_if_word_is_number(checkString);
                if (returnedInt != 0) {
                    return returnedInt;
                }
            }
            startIndex++;
        }
    }
    else {
        startIndex = length - 1;
        while (startIndex >= 2) {
            std::string checkString = "";
            checkString += wordCheck[startIndex];
            checkString += wordCheck[startIndex - 1];
            checkString += wordCheck[startIndex - 2];

            returnedInt = check_if_word_is_number(checkString);
            if (returnedInt != 0) {
                return returnedInt;
            }
            if (startIndex >= 3) {
                checkString += wordCheck[startIndex - 3];
                returnedInt = check_if_word_is_number(checkString);
                if (returnedInt != 0) {
                    return returnedInt;
                }
            }
            if (startIndex >= 4) {
                checkString += wordCheck[startIndex - 4];
                returnedInt = check_if_word_is_number(checkString);
                if (returnedInt != 0) {
                    return returnedInt;
                }
            }
            startIndex--;
        }
    }

    return 'a';
}

void day_1() {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Starting Day 1 program!" << std::endl;

    std::fstream file;
    std::string line;
    std::string wordCheckToCheck;

    char foundNumbers[2];
    int sum = 0;

    file.open("Day1/input.txt", std::ios::in);

    if (file.is_open()) {
        while (getline(file, line)) {
            // Find first number
            wordCheckToCheck = "";
            for (int i = 0; i < line.length(); i++) {
                if (isdigit(line[i])) {
                    foundNumbers[0] = line[i];
                    break;
                }
                else {
                    wordCheckToCheck += line[i];
                    foundNumbers[0] = disassemble_string(wordCheckToCheck, true);
                    if (foundNumbers[0] != 'a') {
                        foundNumbers[0] += 48;
                        break;
                    }
                }
            }

            // Find second number
            wordCheckToCheck = "";
            for (int i = line.length() - 1; i >= 0; i--) {
                if (isdigit(line[i])) {
                    foundNumbers[1] = line[i];
                    break;
                }
                else {
                    wordCheckToCheck += line[i];
                    foundNumbers[1] = disassemble_string(wordCheckToCheck, false);
                    if (foundNumbers[1] != 'a') {
                        foundNumbers[1] += 48;
                        break;
                    }
                }
            }

            std::cout << foundNumbers[0] << "; " << foundNumbers[1] << std::endl;
            sum += ((int)foundNumbers[0] - 48) * 10 + ((int)foundNumbers[1] - 48);
        }
        file.close();        
    }

    std::cout << "Result is: " << sum << std::endl;
}