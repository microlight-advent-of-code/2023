#pragma once
#include <vector>
#include <string>
#include <algorithm>

namespace Constants {
	const std::vector<std::string> NUMBERS_AS_WORDS = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };

    std::vector<std::string> splitString(const std::string& input, char delimiter) {
        std::vector<std::string> substrings;
        size_t start = 0, end = 0;

        while ((end = input.find(delimiter, start)) != std::string::npos) {
            substrings.push_back(input.substr(start, end - start));
            start = end + 1;
        }

        substrings.push_back(input.substr(start));

        return substrings;
    }
    template<typename T>
    bool contains_element(const std::vector<T>& vec, const T& element) {
        auto it = std::find(vec.begin(), vec.end(), element);
        return it != vec.end();
    }
    // Function to trim spaces from the beginning and end of a string
    std::string trim_spaces_edges(const std::string& str) {
        size_t start = str.find_first_not_of(" \t\n\r");
        if (start == std::string::npos) {
            return "";
        }

        size_t end = str.find_last_not_of(" \t\n\r");
        return str.substr(start, end - start + 1);
    }
    std::string removeDuplicateSpaces(const std::string& input) {
        std::string result;
        bool spaceEncountered = false;

        for (char c : input) {
            if (std::isspace(c)) {
                if (!spaceEncountered) {
                    result += ' ';
                    spaceEncountered = true;
                }
            }
            else {
                result += c;
                spaceEncountered = false;
            }
        }

        return result;
    }
}
